﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour {

    private BuildManager buildManager;

    public Toggle turretItem, laserBeamerItem, missileLauncherItem;

    private Button highlightedItem; 

	// Use this for initialization
	void Start () {
        buildManager = BuildManager.instance;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SelectStandardTower()
    {
        buildManager.SetTowerToBuild(buildManager.standardTowerPrefab);
        turretItem.Select();
    }

    public void SelectLaserBeamer()
    {
        buildManager.SetTowerToBuild(buildManager.laserBeamerPrefab);
    }

    public void SelectMissileLauncher()
    {
        buildManager.SetTowerToBuild(buildManager.missileLauncherPrefab);
    }
}
