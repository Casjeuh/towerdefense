﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICameraSystem
{
	float GetZoomFactor();

	Vector3 GetMovementDirection();

	float GetRotationDirection();

	void Test();
}
