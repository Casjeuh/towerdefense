﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndroidCamera : ICameraSystem, IInjectable
{
	float lastZoomDistance, lastRotationAngle;

	public float GetZoomFactor()
	{
		float result = 0f;

		if(Input.touchCount == 2)
		{

			float newDistance = (Input.GetTouch(0).position - Input.GetTouch(1).position).magnitude;
			result = newDistance - lastZoomDistance;
			lastZoomDistance = newDistance;
		}else
		{
			lastZoomDistance = 0;
		}

		return result / 10f;
	}

	public Vector3 GetMovementDirection()
	{
		Vector3 result = Vector3.zero;

		if(Input.touchCount == 1)
		{
			Touch t = Input.GetTouch(0);
			if(Input.GetTouch(0).phase == TouchPhase.Moved)
			{
				result = t.deltaPosition;
			}
		}

		return result;
	}

	public float GetRotationDirection()
	{
		return 0;
		float result = 0f;
		if(Input.touchCount == 2)
		{

			float newAngle = Vector2.Angle(Vector2.up, (Input.GetTouch(0).position - Input.GetTouch(1).position).normalized);
			result = newAngle - lastRotationAngle;
			lastRotationAngle = newAngle;
		}else
		{
			lastRotationAngle = 0;
		}


		return result / 10f;
	}

	public void Test(){
		Debug.Log("Android Camera!");
	}
}