﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PCCamera : ICameraSystem, IInjectable
{
	private Vector3 lastTranslateMousePosition, lastRotateMousePosition;
	public float GetZoomFactor()
	{
		return Input.mouseScrollDelta.y;
	}

	public Vector3 GetMovementDirection()
	{		
		 if (!Input.GetMouseButton(1))
			{
				lastTranslateMousePosition = Input.mousePosition;
				return Vector3.zero;
			}

		Vector3 result = Input.mousePosition - lastTranslateMousePosition;
		lastTranslateMousePosition = Input.mousePosition;
		return result;
	}

	public float GetRotationDirection()
	{
		
		 if (!Input.GetMouseButton(2))
			{
				lastRotateMousePosition = Input.mousePosition;
				return 0;
			}

		Vector3 result = Input.mousePosition - lastRotateMousePosition;
		lastRotateMousePosition = Input.mousePosition;
		return result.x;
	}

	public void Test(){
		Debug.Log("PC Camera");
	}
}
