﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[InjectableClass]
public class CameraController : MonoBehaviour
{

    public float panSpeed = 0.01f;
    public float rotationAngle = 3f, rotationSpeed = 1f;
    public float minZoom = 1f, maxZoom = 10f, zoomSpeed = 5f;

    private Vector3 lastMousePosition;
    private Camera isometricCamera;
    private ICameraSystem cameraSystem;

    // Use this for initialization
    void Start()
    {
        isometricCamera = Camera.main;
		cameraSystem.Test();
    }

    [InjectableConstructor]
    public void Init(ICameraSystem cameraSystem)
    {
        this.cameraSystem = cameraSystem;
    }

    // Update is called once per frame
    void Update()
    {
        float rotateDelta = cameraSystem.GetRotationDirection();
        Vector3 translateDelta = cameraSystem.GetMovementDirection();

        float y = transform.position.y;

        transform.Translate(new Vector3(-translateDelta.x * panSpeed, 0, -translateDelta.y * panSpeed));
        transform.position = new Vector3(transform.position.x, y, transform.position.z);

        Transform rotateTransform = this.transform;
        if (rotateDelta < 0)
            rotateTransform.RotateAround(rotateTransform.position, Vector3.up, -rotationAngle);
        else if (rotateDelta > 0)
            rotateTransform.RotateAround(rotateTransform.position, Vector3.up, rotationAngle);

        Vector3 newRotation = Quaternion.Lerp(transform.rotation, rotateTransform.rotation, Time.deltaTime * rotationSpeed).eulerAngles;
        transform.rotation = Quaternion.Euler(30, newRotation.y, 0);

        float zoomFactor = cameraSystem.GetZoomFactor();
        if (zoomFactor > 0)
            isometricCamera.orthographicSize = Mathf.Max(isometricCamera.orthographicSize - zoomSpeed * Time.deltaTime, minZoom);
        else if (zoomFactor < 0)
            isometricCamera.orthographicSize = Mathf.Min(isometricCamera.orthographicSize + zoomSpeed * Time.deltaTime, maxZoom);

    }
}