﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveDatabase : MonoBehaviour {

    public static WaveDatabase instance;

    [Header("EnemyPrefabs")]
    public GameObject standardEnemyPrefab;
    public GameObject fastEnemyPrefab;
    public GameObject heavyEnemyPrefab;
    public GameObject veryHeavyEnemyPrefab;

    [HideInInspector()]
    public int amountOfWaves;

    private Dictionary<int, List<GameObject>> waveDictionary = new Dictionary<int, List<GameObject>>();

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        InstantiateWaves();
    }

    private void InstantiateWaves()
    {
        List<GameObject> wave = new List<GameObject>();
        wave.Add(standardEnemyPrefab);
        wave.Add(standardEnemyPrefab);
        wave.Add(standardEnemyPrefab);
        waveDictionary.Add(0, wave);

        wave = new List<GameObject>();
        wave.Add(standardEnemyPrefab);
        wave.Add(standardEnemyPrefab);
        wave.Add(standardEnemyPrefab);
        wave.Add(fastEnemyPrefab);
        waveDictionary.Add(1, wave);
        
        wave = new List<GameObject>();
        wave.Add(standardEnemyPrefab);
        wave.Add(standardEnemyPrefab);
        wave.Add(standardEnemyPrefab);
        wave.Add(heavyEnemyPrefab);
        waveDictionary.Add(2, wave);
        
        wave = new List<GameObject>();
        wave.Add(standardEnemyPrefab);
        wave.Add(standardEnemyPrefab);
        wave.Add(standardEnemyPrefab);
        wave.Add(fastEnemyPrefab);
        wave.Add(heavyEnemyPrefab);
        waveDictionary.Add(3, wave);
        
        wave = new List<GameObject>();
        wave.Add(standardEnemyPrefab);
        wave.Add(standardEnemyPrefab);
        wave.Add(standardEnemyPrefab);
        wave.Add(fastEnemyPrefab);
        wave.Add(fastEnemyPrefab);
        wave.Add(fastEnemyPrefab);
        waveDictionary.Add(4, wave);

        wave = new List<GameObject>();
        wave.Add(veryHeavyEnemyPrefab);
        waveDictionary.Add(5, wave);

        for(int i = 6; i < 100; i++)
        {
            wave = new List<GameObject>();
            for(int k = 0; k < i; k++)
                wave.Add(standardEnemyPrefab);
            waveDictionary.Add(i, wave);
        }

        amountOfWaves = waveDictionary.Count;
    }

    public List<GameObject> GetWave(int waveIndex)
    {
        return waveDictionary[waveIndex - 1];
    }
}