﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {

    [Header("Attributes")]
    public int cashWorth = 2;
    public int livesWorth = 1;
    public float startHealth = 200;
    public float startSpeed = 5;

    private float health;
    private EnemyNavigation enemyNavigation;

    [Header("Unity Setup")]
    public Image healthBar;

    // Use this for initialization
    void Start () {
        health = startHealth;

        //Rescale health bar canvas to always be the same size regardless of enemy scale
        float canvasScale = 0.00675f / transform.localScale.x;
        transform.GetChild(0).transform.localScale = new Vector3(canvasScale, canvasScale, canvasScale);

        enemyNavigation = GetComponent<EnemyNavigation>();
        enemyNavigation.agent.speed = startSpeed;
    }
	
	// Update is called once per frame
	void Update () {
        enemyNavigation.agent.speed = startSpeed;
	}

    public void TakeDamage(int amount)
    {
        health -= amount;

        healthBar.fillAmount = health / startHealth;

        if (health <= 0)
            Die();
    }

    public void Slow(float factor)
    {
        enemyNavigation.agent.speed = startSpeed * (1f - factor); 
    }

    public void Die()
    {
        BuildManager.instance.AddCash(cashWorth);
        Destroy(gameObject);
    }
}
