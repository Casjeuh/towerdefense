﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyNavigation : MonoBehaviour {

    [HideInInspector()]
    public NavigationField navField;
    [HideInInspector()]
    public NavMeshAgent agent;

    private Transform goal;
    private NavigationPath path;
    private int currentNode = 0;
    private bool triggered = false;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Use this for initialization
    void Start () {
        path = navField.ShortestPath();
        SetNextGoal();
    }

    // Update is called once per frame
    void Update () {
	}

    public void CalculateNewPath(NavigationTile blockedTile)
    {
        NavigationTile current = path.CurrentNavigationTile;
        if(current == null)
        {
            CalculateNewPath(blockedTile);
            return;
        }
        else if (path.CurrentNavigationTile == blockedTile)
        {
            path = navField.ShortestPath(path.PreviousNavigationTile);
            SetNextGoal();
        }
        else
            path = navField.ShortestPath(path.CurrentNavigationTile);
    }

    private void SetNextGoal()
    {
        goal = path.NextTransform();
        if (goal == null || agent == null)
            return;
        agent.destination = goal.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Tile" && !triggered)
        {
            SetNextGoal();
            triggered = true;
        }
        else if (other.tag == "Goal")
        {
            GameManager.instance.RemoveLives(GetComponent<Enemy>().livesWorth);
            Destroy(gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        triggered = false;
    }
}


