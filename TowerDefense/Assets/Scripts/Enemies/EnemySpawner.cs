﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour {

    public static EnemySpawner instance;

    void Awake()
    {
        instance = this;
    }

    [Header("Attributes")]
    public float timeBetweenWaves = 5.5f;

    [Header("Unity Setup")]
    public Transform spawnPoint;
    public Text waveText;

    private int waveIndex = 1;
    private List<EnemyNavigation> enemies;
    private float spawnTimer = 5;
    private WaveDatabase waveDatabase;
    private NavigationField navField;

    // Use this for initialization
    void Start () {
        enemies = new List<EnemyNavigation>();
        navField = NavigationField.instance;
        waveDatabase = WaveDatabase.instance;
	}
	
	// Update is called once per frame
	void Update () {
        if(spawnTimer <= 0)
        {
            StartCoroutine(SpawnWave());
            spawnTimer = timeBetweenWaves;
        }

        spawnTimer -= Time.deltaTime;

        waveText.text = "Next in " + Mathf.Round(spawnTimer).ToString();
    }

    private IEnumerator SpawnWave()
    {   
        List<GameObject> wave = WaveDatabase.instance.GetWave(waveIndex);

        for(int i = 0; i < wave.Count; i++)
        {
            GameObject newEnemy = Instantiate(wave[i], spawnPoint.position, Quaternion.identity);
            EnemyNavigation enemyNav = newEnemy.GetComponent<EnemyNavigation>();
            enemyNav.navField = navField;
            enemies.Add(enemyNav);
            yield return new WaitForSeconds(0.5f);
        }

        waveIndex++;

        yield return null;
    }
    
    public void RecalculatePaths(NavigationTile blockedTile)
    {
        //Build a new enemy list and only add non-destroyed enemies to it, to avoid NullReferenceExceptions
        List<EnemyNavigation> newEnemies = new List<EnemyNavigation>();

        foreach (EnemyNavigation enemy in enemies)
        {
            if (enemy == null)
                continue;
            
            enemy.CalculateNewPath(blockedTile);
            newEnemies.Add(enemy);
        }

        enemies = newEnemies;
    }
}
