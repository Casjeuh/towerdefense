﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationPath {

    private LinkedList<NavigationTile> path;
    private LinkedListNode<NavigationTile> currentTile;

	// Use this for initialization
	public NavigationPath ()
    {
        path = new LinkedList<NavigationTile>();
	}

    public Transform NextTransform()
    {
        if (currentTile.Next == null)
            return null;

        currentTile = currentTile.Next;
        return currentTile.Value.transform;
    }

    public void AddFirst(NavigationTile tile)
    {
        path.AddFirst(tile);

        currentTile = path.First;
    }

    public NavigationTile CurrentNavigationTile
    {
        get
        {
            if (currentTile == null)
                currentTile = path.First;
            return currentTile.Value;
        }
    }

    public NavigationTile PreviousNavigationTile
    {
        get { return currentTile.Previous.Value; }
    }

    public bool ReachesGoal
    {
        get { return path.Count > 0 && path.Last.Value.tag == "Goal"; }
    }
}
