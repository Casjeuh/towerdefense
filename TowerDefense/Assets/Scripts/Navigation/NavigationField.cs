﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationField : MonoBehaviour
{
    public static NavigationField instance;
    void Awake()
    {
        instance = this;
    }

    public int xNavigationTiles, yNavigationTiles;

    private NavigationTile[,] tiles;
    private int childCounter;
    private NavigationTile spawnTile, goalTile;

    // Use this for initialization
    void Start()
    {
        childCounter = 0;

        tiles = new NavigationTile[xNavigationTiles, yNavigationTiles];

        for (int y = 0; y < yNavigationTiles; y++)
            for (int x = 0; x < xNavigationTiles; x++)
            {
                tiles[x, y] = transform.GetChild(childCounter).GetComponent<NavigationTile>();
                tiles[x, y].number = childCounter++;
            }

        spawnTile = transform.GetChild(childCounter).GetComponent<NavigationTile>();
        spawnTile.number = childCounter++;
        goalTile = transform.GetChild(childCounter).GetComponent<NavigationTile>();
        goalTile.number = childCounter++;

        tiles[0, 0].Connect(spawnTile);
        tiles[xNavigationTiles - 1, yNavigationTiles - 1].Connect(goalTile);

        for (int y = 0; y < yNavigationTiles; y++)
            for (int x = 0; x < xNavigationTiles; x++)
            {
                if (y - 1 >= 0)
                    tiles[x, y].neighbours.Add(tiles[x, y - 1]);

                if (y + 1 < yNavigationTiles)
                    tiles[x, y].neighbours.Add(tiles[x, y + 1]);

                if (x - 1 >= 0)
                    tiles[x, y].neighbours.Add(tiles[x - 1, y]);

                if (x + 1 < xNavigationTiles)
                    tiles[x, y].neighbours.Add(tiles[x + 1, y]);
            }

    }

    /* Calculate the shortest path from start to goal node using BFS
     * If calculating path from spawn to goal, leave parameters blank */
    public NavigationPath ShortestPath(NavigationTile start = null, NavigationTile goal = null)
    {
        if (start == null)
            start = spawnTile;
        if (goal == null)
            goal = goalTile;
        
        NavigationPath result = new NavigationPath();

        Queue<NavigationTile> queue = new Queue<NavigationTile>();
        List<NavigationTile> visited = new List<NavigationTile>();
        NavigationTile[] previous = new NavigationTile[childCounter];

        /* Perform BFS */

        queue.Enqueue(start);

        while (queue.Count != 0)
        {
            NavigationTile current = queue.Dequeue();

            if (!visited.Contains(current))
            {
                visited.Add(current);

                foreach (NavigationTile neighbour in current.neighbours)
                    if (!visited.Contains(neighbour) && !neighbour.occupied)
                    {
                        previous[neighbour.number] = current;
                        queue.Enqueue(neighbour);
                    }

            }
        }

        /* Reconstruct path from previous array */

        if(previous[goal.number] != null)
        {
            result.AddFirst(goal);
            NavigationTile node = previous[goal.number];
            while (node != null)
            {
                result.AddFirst(node);
                node = previous[node.number];
            }
        }

        return result;
    }

    // Update is called once per frame
    void Update()
    {
        foreach (NavigationTile node in tiles)
            foreach(NavigationTile n in node.neighbours)
             Debug.DrawLine(node.transform.position, n.transform.position, Color.red);
    }

    //Check if tower does not block path
    public bool ValidTowerPlacement(NavigationTile tile)
    {
        if (tile.occupied)
            return false;

        tile.occupied = true;
        NavigationPath path = ShortestPath();
        tile.occupied = false;

        if (!path.ReachesGoal)
        {
            Debug.Log("Invalid tower placement");
            return false;
        }

        return true;
    }
}
