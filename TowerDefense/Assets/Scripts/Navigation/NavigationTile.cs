﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationTile : MonoBehaviour {

    public Color hoverColor, invalidHoverColor;

    [HideInInspector()]
    public List<NavigationTile> neighbours;
    [HideInInspector()]
    public int number;
    [HideInInspector()]
    public bool occupied = false;

    private Renderer gRenderer;
    private Color startColor;
    private GameObject tower;
    private BuildManager buildManager;

    // Use this for initialization
    void Start () {
        buildManager = BuildManager.instance;
        gRenderer = GetComponent<Renderer>();
        startColor = gRenderer.material.color;
	}
	
	// Update is called once per frame
	void Update () {
    }

    public void Connect(NavigationTile other)
    {
        if(!neighbours.Contains(other))
        {
            neighbours.Add(other);
            other.neighbours.Add(this);
        }
    }

    public void PlaceTower(GameObject tower)
    {
        tower = (GameObject)Instantiate(tower, transform.position, Quaternion.identity);
        occupied = true;
    }

    private void OnMouseDown()
    {
        buildManager.BuildTower(this);
    }

    void OnMouseEnter()
    {
        if (!buildManager.TowerSelected)
            return;

        buildManager.deselectTowerOnClick = false;

        if(buildManager.ValidTowerPlacement(this))
            gRenderer.material.color = hoverColor;
        else
            gRenderer.material.color = invalidHoverColor;
    }

    void OnMouseExit()
    {
        buildManager.deselectTowerOnClick = true;

        gRenderer.material.color = startColor;
    }
}
