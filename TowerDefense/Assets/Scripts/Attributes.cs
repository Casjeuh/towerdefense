﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.AttributeUsage(System.AttributeTargets.Method, AllowMultiple = false)]
public class InjectableConstructor : System.Attribute {
    
}

[System.AttributeUsage(System.AttributeTargets.Class, AllowMultiple = false)]
public class InjectableClass : System.Attribute
{

}
