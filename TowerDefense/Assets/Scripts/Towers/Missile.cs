﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : Bullet {

    public float explosionRadius = 1.5f;

    protected override void Update()
    {
        base.Update();

        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        transform.LookAt(target.transform);
    }

    protected override void HitTarget()
    {
        Explode();
        Destroy(gameObject);
    }

    private void Explode()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach(Collider collider in colliders)
        {
            if(collider.tag == "Enemy")
            {
                Damage(collider.transform);
            }
        }
    }

    private void Damage(Transform enemy)
    {
        enemy.GetComponent<Enemy>().TakeDamage(damage);
    }
}
