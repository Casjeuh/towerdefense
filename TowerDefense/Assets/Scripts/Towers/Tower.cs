﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {

    [Header("Attributes")]
    public int cost = 5;
    public float range = 2.5f;
    public float fireRate = 1f;

    [Header("Unity Setup")]
    public Transform rotationPart;
    public Transform firePoint;

    protected List<Transform> possibleTargets;
    protected Enemy target;
    protected float turnSpeed = 10f;
    protected float fireCooldown = 0f;

    // Use this for initialization
    protected virtual void Start () {
        possibleTargets = new List<Transform>();

        InvokeRepeating("UpdateTarget", 0f, 0.5f);
	}
	
	// Update is called once per frame
	protected virtual void Update () {
        if (target == null)
            return;

        Vector3 direction = target.transform.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        Vector3 rotation = Quaternion.Lerp(rotationPart.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        rotationPart.rotation = Quaternion.Euler(0, rotation.y, 0);

        if(fireCooldown <= 0)
        {
            Shoot();
            fireCooldown = 1f / fireRate;
        }

        fireCooldown -= Time.deltaTime;

	}

    protected virtual void Shoot()
    {
        return;
    }

    protected void UpdateTarget()
    {
        if(possibleTargets.Count == 0)
        {
            target = null;
            return;
        }

        float shortestDistance = float.MaxValue;
        Transform closestEnemy = null;
        foreach(Transform enemy in possibleTargets)
        {
            if (enemy == null)
                continue;

            float distanceToEnemy = Vector3.Distance(transform.position, enemy.position);
            if(distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                closestEnemy = enemy;
            }
        }

        if(closestEnemy != null)
            target = closestEnemy.GetComponent<Enemy>();
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
            possibleTargets.Add(other.transform);
    }

    protected void OnTriggerExit(Collider other)
    {
        if (other.tag == "Enemy")
            possibleTargets.Remove(other.transform);
    }
}
