﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : Tower {

    [Header("Attributes")]
    public int bulletDamage = 10;
    public float bulletSpeed = 10;

    [Header("Unity Setup")]
    public GameObject bulletPrefab;

    protected override void Shoot()
    {
        GameObject bulletObj = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Bullet bullet = bulletObj.GetComponent<Bullet>();
        bullet.damage = bulletDamage;
        bullet.speed = bulletSpeed;
        bullet.target = target;
    }
}
