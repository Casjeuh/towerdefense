﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBeamer : Tower
{
    [Header("Attributes")]
    public float slowFactor = 0.5f;

    private LineRenderer lineRenderer;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.enabled = false;
    }

    protected override void Update()
    {
        if (target == null)
        {
            lineRenderer.enabled = false;
            return;
        }

        target.Slow(slowFactor);

        lineRenderer.enabled = true;
        lineRenderer.SetPositions(new Vector3[] { firePoint.position, target.transform.position });

        Vector3 direction = target.transform.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        Vector3 rotation = Quaternion.Lerp(rotationPart.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        rotationPart.rotation = Quaternion.Euler(0, rotation.y, 0);
    }
}
