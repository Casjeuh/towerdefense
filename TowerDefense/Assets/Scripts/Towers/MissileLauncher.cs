﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileLauncher : Tower {

    [Header("Attributes")]
    public int bulletDamage = 10;
    public float bulletSpeed = 6;

    [Header("Unity Setup")]
    public GameObject bulletPrefab;

    protected override void Shoot()
    {
        GameObject bulletObj = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Missile bullet = bulletObj.GetComponent<Missile>();
        bullet.damage = bulletDamage;
        bullet.speed = bulletSpeed;
        bullet.target = target;
    }
}
