﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildManager : MonoBehaviour
{
    public static BuildManager instance;

    public GameObject standardTowerPrefab, laserBeamerPrefab, missileLauncherPrefab;
    public Text cashText;
    [HideInInspector()]
    public bool deselectTowerOnClick = true;
    [HideInInspector()]
    public int cash = 10;

    private NavigationField navField;
    private EnemySpawner spawner;
    private GameObject towerToBuild;
    
    void Awake()
    {
        instance = this;
    }


    void Start()
    {
        navField = NavigationField.instance;
        spawner = EnemySpawner.instance;
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0) && deselectTowerOnClick)
        {
            towerToBuild = null;
        }
    }

    public GameObject GetTowerToBuild
    {
        get { return towerToBuild; }
    }

    public void SetTowerToBuild(GameObject tower)
    {
        if(tower.GetComponent<Tower>().cost <= cash)
            towerToBuild = tower;
    }

    public void DeselectTower()
    {
        towerToBuild = null;
    }

    public bool TowerSelected
    {
        get { return towerToBuild != null; }
    }

    public void BuildTower(NavigationTile tile)
    {
        if (!TowerSelected || tile.occupied || !ValidTowerPlacement(tile))
            return;

        RemoveCash(towerToBuild.GetComponent<Tower>().cost);
        tile.PlaceTower(towerToBuild);
        spawner.RecalculatePaths(tile);
    }

    public void AddCash(int amount)
    {
        cash += amount;
        cashText.text = "€" + cash;
    }

    private void RemoveCash(int cost)
    {
        cash -= cost;
        cashText.text = "€" + cash;
    }

    public bool ValidTowerPlacement(NavigationTile tile)
    {
        if(cash < towerToBuild.GetComponent<Tower>().cost)
            return false;            

        return navField.ValidTowerPlacement(tile);
    }
}
