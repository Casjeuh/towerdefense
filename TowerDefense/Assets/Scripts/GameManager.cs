﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public static GameManager instance;

	private void Awake() {
		instance = this;
	}

	public int lives = 20;

	public Text livesText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void RemoveLives(int amount)
	{
		lives -= amount;
		if (lives < 0)
			lives = 0;

		livesText.text = lives + " Lives";
	}
}
