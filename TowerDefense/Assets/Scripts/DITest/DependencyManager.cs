﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DependencyManager : MonoBehaviour {

	MonoBehaviour[] monoBehaviours;
	List<MonoBehaviour> injectables;
	Dictionary<Type, IInjectable> seeder;


	// Use this for initialization
	void Awake () {
		//DEFAULT
#if UNITY_STANDALONE_WIN
        seeder = new Dictionary<Type, IInjectable>
        {
            { typeof(ICameraSystem), new PCCamera() }
        };
#elif UNITY_ANDROID
		seeder = new Dictionary<Type, IInjectable>
		{
			{typeof(ICameraSystem), new AndroidCamera() }
		};
#endif


        monoBehaviours = (MonoBehaviour[])Resources.FindObjectsOfTypeAll(typeof(MonoBehaviour));
		injectables = monoBehaviours.Where(x => System.Attribute.GetCustomAttributes(x.GetType(), typeof(InjectableClass)).Length > 0).ToList();

		foreach(MonoBehaviour t in injectables)
		{
			MethodInfo initMethod = t.GetType().GetMethods().Where(x => x.GetCustomAttributes(typeof(InjectableConstructor), false).Length > 0).FirstOrDefault();
			ParameterInfo[] pars = initMethod.GetParameters();
			List<object> shouldInject = new List<object>();
			foreach(ParameterInfo i in pars)
			{
				if (!seeder.ContainsKey(i.ParameterType))
					shouldInject.Add(null);
				else
					shouldInject.Add(seeder[i.ParameterType]);
			}
			initMethod.Invoke(t, shouldInject.ToArray());

		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
