﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IControlSystem
{
	void Test();
}

public class PCControls : IControlSystem, IInjectable
{
	public void Test()
	{
		Debug.Log("PC Controls!");
	}
}

public class MobileControls : IControlSystem, IInjectable
{
	public void Test()
	{
		Debug.Log("Mobile Controls!");
	}
}